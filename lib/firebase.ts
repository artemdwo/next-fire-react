import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/storage";
import "firebase/compat/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCckloBl6qcuz8ZYuRgRDfZitZeC2Ig4jI",
  authDomain: "next-fire-react.firebaseapp.com",
  projectId: "next-fire-react",
  storageBucket: "next-fire-react.appspot.com",
  messagingSenderId: "3440298694",
  appId: "1:3440298694:web:001743c8c56bbabca1e5c7",
  measurementId: "G-1S4KGK6CYV",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)

  if (process.env.NODE_ENV === "development") {
    firebase.firestore().settings({
      host: "localhost:8080",
      ssl: false,
      experimentalForceLongPolling: true
    });
    firebase.auth().useEmulator('http://localhost:9099')
    firebase.storage().useEmulator('localhost', 9199)
  }
}

export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider()
export const firestore = firebase.firestore();
export const storage = firebase.storage();

export async function getUserWithUsername(username: string) {
  const ref = firestore.collection('users')
  const query = ref.where('username', '==', username).limit(1)
  const doc = (await query.get()).docs[0]

  return doc
}

export async function postToJson(doc: firebase.firestore.DocumentSnapshot) {
  const data = doc.data()

  return {
    ...data,
    createdAt: data.createdAt.toMillis(),
    updatedAt: data.updatedAt.toMillis()
  }
}
