import UserProfile from "../../components/UserProfile";
import PostFeed from "../../components/PostFeed";
import { getUserWithUsername, postToJson } from "../../lib/firebase";

export async function getServerSideProps({ query }) {
  const { username } = query;
  const userDoc = await getUserWithUsername(username);

  let user: {} = null;
  let posts = null;

  if (userDoc) {
    user = userDoc.data();
    const PostsQuery = userDoc.ref
      .collection("posts")
      .where("published", "==", true)
      .orderBy("createdAt", "desc")
      .limit(5);
    posts = (await PostsQuery.get()).docs.map(postToJson);
  }

  return {
    props: { user, posts },
  };
}

export default function UserPage({ user, posts }) {
  return (
    <main>
      <h1>Username page</h1>
      <UserProfile user={user} />
      <PostFeed posts={posts} admin={false} />
    </main>
  );
}
